const path = require('path');
module.exports = {  
    entry: { home: ['./js/app.js'], story: ['./js/story.js']},
    output: {
        path: path.resolve(__dirname, 'dist/js'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                  loader: "babel-loader",
                  options: { presets: ["es2015"] }
                },
                exclude: /node_modules/                
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS
                ],
                exclude: /node_modules/
            },
            {
                test: /\.(jpe?g|png|woff|woff2|eot|ttf|otf|svg)(\?[a-z0-9=.]+)?$/, 
                loader: 'url-loader?limit=100000'
            }
        ]
    },
    devServer:{
        contentBase: __dirname + '/',
        host: '0.0.0.0',
        open: true,
        compress: true,
        port: 8899
    }
}
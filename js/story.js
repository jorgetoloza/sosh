import './../sass/story.scss'; 

import { TweenMax, Power4 } from "gsap/TweenMax";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";

import ScrollCtrl from './scroll';

var $header = document.getElementById('mainHeader');
var scrollCtrl;

function init() {
    scrollCtrl = new ScrollCtrl(scroll);
    if (window.addEventListener) window.addEventListener('DOMMouseScroll', function(e) {scrollCtrl.preventDefault(e)}, {passive: true})
    window.onwheel = function(e) { scrollCtrl.preventDefault(e) } // modern standard
    window.onmousewheel = document.onmousewheel = function(e) {  scrollCtrl.preventDefault(e) } // older browsers, IE

    window.addEventListener('touchmove', scrollCtrl.touchMove, {passive: true})
    window.addEventListener('touchstart', scrollCtrl.touchStart, {passive: true})
    window.addEventListener('touchend', scrollCtrl.touchEnd, {passive: true});    
}

function scroll(x, y, direction, event){
    var delta = y;
    var limit = window.innerHeight / 5

    if (delta < 0) {
        if (delta < -limit) delta = -limit
    } else {
        if (delta > limit) delta = limit
    }

    var scrollTop = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0)
    var finalScroll = scrollTop - delta;
    TweenMax.to(window, 0.8, {
        scrollTo: { y: finalScroll, autoKill: true },
        ease: Power4.easeOut,
        overwrite: 5
    })
}
init();
window.testBrowser = function (browser) {
    var result

    switch (browser) {
    case 'safari':
        result = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === '[object SafariRemoteNotification]' })(!window['safari'] || (typeof window.safari !== 'undefined' && window.safari.pushNotification))
        break
    case 'safari mobile':
        result = /iPhone/i.test(navigator.userAgent) && /Safari/i.test(navigator.userAgent)
        break
    case 'samsung':
        result = /SamsungBrowser/.test(navigator.userAgent)
        break
    case 'chrome':
        result = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !/SamsungBrowser/.test(navigator.userAgent)
        break
    case 'chrome mobile':
        result = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !/SamsungBrowser/.test(navigator.userAgent) && !window.chrome.webstore
        break
    case 'firefox mobile':
        result = !/Chrome/.test(navigator.userAgent) && /Mozilla/.test(navigator.userAgent) && /Firefox/.test(navigator.userAgent) && /Mobile/.test(navigator.userAgent)
        break
    case 'firefox':
        result = !/Chrome/.test(navigator.userAgent) && /Mozilla/.test(navigator.userAgent) && /Firefox/.test(navigator.userAgent)
        break
    case 'ie':
        result = /MSIE/.test(window.navigator.userAgent) || /NET/.test(window.navigator.userAgent);
        break;
    case 'edge':
        result = /Edge/.test(window.navigator.userAgent);
    default:
        result = false
        break
    }
    return result
}

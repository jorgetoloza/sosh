import './../sass/home.scss'; 

import { TweenMax, TimelineLite , Power4 } from "gsap/TweenMax";
import ScrollCtrl from './scroll';

var $headerIntro = document.getElementById('headerIntro');
var $header = document.getElementById('mainHeader');

var $sliderNav = document.getElementById('circles');
var $mainSkip = document.getElementById('mainSkip');
var $bus = document.getElementById('bus');


var $scrollMask = document.getElementById('scrollMask');
var $content = $scrollMask.querySelector('#container');

var $verticalSection = document.querySelector('.vertical-scroll')

var $prev = document.getElementById('prev');
var $next = document.getElementById('next');

var scrollCtrl;
var horizontalMode = true;
var timelines = [];

function init() {
    Array.from(document.querySelectorAll('section .title-h1')).forEach($title => {
        var html = '';
        Array.from($title.innerText).forEach(c => {
            html += '<span>' + c + '</span>'
        });
        $title.innerHTML = html;
    });
    /*TweenMax.set($prev, {opacity: 0});
    TweenMax.set($next, {opacity: 0, top: window.innerHeight * 0.5 - $prev.clientHeight * 0.5});*/
    timelines.push((() => {
        var $s = document.getElementById('intro');
        var t = new TimelineLite({paused: true});
        t.add([
            TweenMax.to($header, 1.2, {opacity: 0, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.img-container .mask'), 4.2, {width: '0%', opacity: 1}, {width: '55vw', opacity: 1, ease: Power4.easeOut}),
            //TweenMax.to($prev, 1.2, {opacity: 0, ease: Power4.easeOut})
        ]);
        return t;
    })());
    timelines.push((() => {
        var $s = document.getElementById('harassment');
        var t = new TimelineLite({paused: true});
        var delay = -1;
        /*t.add([
            TweenMax.to($header, 1.2, {opacity: 1, ease: Power4.easeOut}),
            TweenMax.to($prev, 1.2, {opacity: 1, ease: Power4.easeOut}),
            TweenMax.to($next, 1.2, {opacity: 1, top: 200, ease: Power4.easeOut})
        ])*/
        t.add(Array.from($s.querySelectorAll('.title-h1 span')).map(($c, i) => {
            delay += 0.05;
            return TweenMax.fromTo($c, 1 + (i * 0.2) , {x: '100%', opacity: 0}, {x: '0%', opacity: 1, delay: delay, ease: Power4.easeOut});
        }));
        
        delay = -4.2;
        t.add([
            TweenMax.fromTo($s.querySelector('.col:nth-child(1)'), 2.8, {x: '30%', opacity: 0}, {x: '0%', opacity: 1, delay: delay -0.8, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(2)'), 3.2, {x: '30%', opacity: 0}, {x: '0%', opacity: 1, delay: delay -0.5, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(3)'), 3.4, {x: '30%', opacity: 0}, {x: '0%', opacity: 1, delay: delay -0.2, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(1) hr'), 2.8, {scaleY: 0, opacity: 0}, {scaleY: 1, opacity: 1, delay: -4, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(2) hr'), 3.5, {scaleY: 0, opacity: 0}, {scaleY: 1, opacity: 1, delay: -3.5, ease: Power4.easeOut}),
        ]);
        
        //t.delay(0.5);
        return t;    
        
    })());
    timelines.push((() => {
        var $s = document.getElementById('srilanka');
        var t = new TimelineLite({paused: true});
        var delay = 0;
        t.add(Array.from($s.querySelectorAll('.title-h1 span')).map(($c, i) => {
            delay += 0.05;
            return TweenMax.fromTo($c, 1 + (i * 0.2), {x: '100%', opacity: 0}, {x: '0%', opacity: 1, delay: delay, ease: Power4.easeOut});
        }));
        delay = -2.2;
        t.add([
            TweenMax.fromTo($s.querySelector('.col:nth-child(1)'), 1.8, {x: '30%', opacity: 0}, {x: '0%', opacity: 1, delay: delay -1, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(2)'), 2.8, {x: '30%', opacity: 0}, {x: '0%', opacity: 1, delay: delay -0.8, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(3)'), 3.8, {x: '30%', opacity: 0}, {x: '0%', opacity: 1, delay: delay -0.2, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(1) hr'), 2.8, {scaleY: 0, opacity: 0}, {scaleY: 1, opacity: 1, delay: delay -0.4, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.col:nth-child(2) hr'), 3.5, {scaleY: 0, opacity: 0}, {scaleY: 1, opacity: 1, delay: delay -0.2, ease: Power4.easeOut})
        ]);
        return t;    
    })());
    timelines.push((() => {
        var $s = document.getElementById('video');
        var t = new TimelineLite({paused: true});
        t.add(
            TweenMax.fromTo($s.querySelector('.video-container'), 1.2, {x: '50%'}, {x: '0%', ease: Power4.easeOut})
        );    
        return t;
    })());
    timelines.push((() => {   
        var t = new TimelineLite({paused: true});
        //t.add(TweenMax.to($bus, 0.5, {x: 0, opacity: 0, delay: -0.5, ease: Power4.easeOut}));
        var delay = 0;
        t.add(Array.from($verticalSection.querySelectorAll('.title-h1 span')).map(($c, i) => {
            delay += 0.05;
            return TweenMax.fromTo($c, 1 + (i * 0.2) , {x: '100%', opacity: 0}, {x: '0%', opacity: 1, delay: delay, ease: Power4.easeOut});
        }));
        delay = -4.2;
        t.add([
            TweenMax.to($next, 1.2, {opacity: 0, delay: delay, ease: Power4.easeOut}),
            //TweenMax.fromTo($verticalSection.querySelector('h1'), 1.2, {opacity: 0, y: '50%'}, {opacity: 1, y: '0%', delay: 0.3, ease: Power4.easeOut}),
            TweenMax.fromTo(document.getElementById('mainLine'), 3.5, {width: '-=' + window.innerWidth * 0.5}, {width: '+=' + window.innerWidth * 0.5, delay: delay - 0.3, ease: Power4.easeOut})
        ]);    
        return t;    
    })());

    scrollCtrl = new ScrollCtrl(scroll);
    if (window.addEventListener) window.addEventListener('DOMMouseScroll', function(e) {scrollCtrl.preventDefault(e)}, {passive: true})
    window.onwheel = function(e) { scrollCtrl.preventDefault(e) } // modern standard
    window.onmousewheel = document.onmousewheel = function(e) {  scrollCtrl.preventDefault(e) } // older browsers, IE
    TweenMax.set($content, {x: 0});
    TweenMax.to($bus, 0.01, {x: -240, opacity: 0});
    TweenMax.to(Array.from($bus.querySelectorAll('.wheel')), 0.01, {transformOrigin: '50% 50%'});    
   

    window.addEventListener('touchmove', scrollCtrl.touchMove, {passive: true})
    window.addEventListener('touchstart', scrollCtrl.touchStart, {passive: true})
    window.addEventListener('touchend', scrollCtrl.touchEnd, {passive: true});

    var loadingAnimation = (() => {   
        var $s = document.getElementById('intro');

        TweenMax.to($prev, 0.01, {opacity: 0, ease: Power4.easeOut});
        TweenMax.to($header, 0.01, {opacity: 0, ease: Power4.easeOut});
        TweenMax.to($mainSkip, 0.01, {opacity: 0, ease: Power4.easeOut});
        TweenMax.to(Array.from($s.querySelectorAll('.title-h1 span')), 0.01, {opacity: 0, y: -100});


        TweenMax.set($s.querySelector('h2'), {opacity: 0, y: '50%'});
        TweenMax.set($s.querySelector('.skip'), {opacity: 0});
        TweenMax.set($s.querySelector('.img-container'), {width: '100vw'});
        TweenMax.set($s.querySelector('.img-container .mask'), {width: '100vw', opacity: 1});
        TweenMax.set($s.querySelector('.img-container .img'), {width: '100vw'});

        var t = new TimelineLite({paused: true});
        var delay = 0;
        t.add([
            TweenMax.fromTo($s.querySelector('.img-container'), 1.2, {width: '100vw'}, {width: '50vw', height: '-=' + 80, delay: 0, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.img-container .mask'), 1.2, {width: '100vw', opacity: 1}, {width: '50vw', opacity: 1, delay: 0, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.img-container .img'), 1.2, {width: '100vw'}, {width: '50vw', delay: 0, ease: Power4.easeOut}),
            TweenMax.fromTo($headerIntro, 0.4, {opacity: 1, y: '0%'}, {opacity: 0, y: '-100%', delay: 0, ease: Power4.easeOut})
        ]);
        delay = 0;

        t.add(Array.from($s.querySelectorAll('.title-h1 span')).map(($c, i) => {
            delay += 0.05;
            return TweenMax.fromTo($c, 1 + (i * 0.2) , {x: '100%', opacity: 0}, {x: '0%', opacity: 1, delay: delay, ease: Power4.easeOut});
        }));
        delay = -2.2;
        t.add([
            
            //TweenMax.fromTo($s.querySelector('h1'), 1.8, {opacity: 0, x: '100%'}, {opacity: 1, x: '0%', delay: delay - 0.8, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('h2'), 3, {opacity: 0, y: '50%'}, {opacity: 1, y: '0%', delay: delay - 1, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('.skip'), 3, {opacity: 0, x: '50%'}, {opacity: 1, x: '0%', delay: delay - 0.4, ease: Power4.easeOut}),
            TweenMax.fromTo($s.querySelector('hr'), 2.8, {opacity: 0, width: 0}, {opacity: 1, width: 400, delay: delay - 0.2, ease: Power4.easeOut}),
            TweenMax.fromTo($next, 2, {opacity: 0, scale: 0.8, top: window.innerHeight * 0.5 - $prev.clientHeight * 0.5}, {opacity: 1, scale: 1, delay: delay + 0.2, ease: Power4.easeOut})
        ]);
        return t;
    })();
    loadingAnimation.delay(2);
    setTimeout(()=>{ loading = false}, (loadingAnimation._totalDuration - 3) * 1000);
    loadingAnimation.play();
    
    $prev.addEventListener('click', () => { move(true) });
    $next.addEventListener('click', () => { move() });

    Array.from($sliderNav.querySelectorAll('.circle')).forEach(($c) => {
        $c.addEventListener('click', (e) => { 
            var $slide = e.target;
            var i = Array.from($sliderNav.children).indexOf($slide) + 1;
            var prev = i < currentSection;
            if(Math.abs(i - currentSection) > 1){
                currentSection += i - 1 - currentSection;
                if(prev)
                    currentSection += 2;
            }
            move(prev);
        });
    });


    document.addEventListener('keydown', (e) => {
        horizontalMode = (currentSection - 1) != verticalSectionIndex;
        var delta = 1.75 * 10;
        if(e.keyCode == 37 || e.keyCode == 38){
            if(horizontalMode)
                move(true);
            else
                scroll(delta, delta, {y: 'up', x: 'left'});
        }else if(e.keyCode == 39 || e.keyCode == 40){
            if(horizontalMode)
                move();
            else
                scroll(-delta, -delta, {y: 'down', x: 'right'});
        }

    });
    setTimeout(()=>{
    $verticalSection.querySelector('.bg').style.height = $verticalSection.scrollHeight + 'px';}, 500);
}
var currentSection = 1;
var verticalSectionIndex = Array.from(document.querySelectorAll('section')).indexOf($verticalSection);
var moving = false;
var offset = 116;
var loading = true;
var hiddingIntroHeader = false;
function scroll(x, y, direction, event){
    if(moving || loading) return;
    var delta = y ? y : x;
    horizontalMode = (currentSection - 1) != verticalSectionIndex;
    //console.log(currentSection, verticalSectionIndex, horizontalMode)
    if(!horizontalMode){
        var $scrollElem = $content;
        var $contentElement = $verticalSection;
        var top = $scrollElem._gsTransform.y + delta * 7 >= 0 && (direction.y == 'up' || direction.x == 'left');
        var bottom = Math.abs($scrollElem._gsTransform.y + delta * 7) > $contentElement.scrollHeight - window.innerHeight && (direction.y == 'down' || direction.x == 'right');

        var y = top ? 0 : ('+=' + (delta * 7));
        if(bottom)
            y = -($contentElement.scrollHeight - window.innerHeight);

        if(top && $scrollElem._gsTransform.y >= 0){ 
            //moving = true;
            document.body.style.background = '#56281D';
            move(true); 
        }
        if($scrollElem._gsTransform.y + delta * 7 < 0 && (direction.y == 'down' || direction.x == 'right')){
            $header.classList.add('sticky');
        }else{
            $header.classList.remove('sticky');
        }
            
        
        TweenMax.to($scrollElem, top ? 0.5 : 1.2, {y: y, ease: Power4.easeOut, onComplete: top ? () => {
            document.body.style.background = '#A3ADB6';
        } : null});    
        
    }else{
        move(direction.x == 'left' || direction.y == 'up');
    }
}
function move(prev){
    if(moving || (prev && currentSection < 2) || (!prev && !horizontalMode)) return;
    moving = true; 
    $header.classList.remove('sticky');
    TweenMax.to($mainSkip, 0.5, {opacity: 0, x: '-50%', ease: Power4.easeOut});
    var options = {ease: Power4.easeOut};
    var ti;
    var activeIndex = (prev ? currentSection - 1 : currentSection + 1) - 1;
    var $active = $sliderNav.children[activeIndex];
    $active.classList.add('active');
    $active.classList.remove('seen');
    var slides = Array.from($sliderNav.children);
    slides.forEach(($s, i) => {
        if(i < activeIndex){
            $s.classList.add('seen');
            $s.classList.remove('active');
        }else if(i > activeIndex)
            $s.classList.remove('seen', 'active');
    });
    if(prev){
        ti = currentSection-2;
        options.y = 0;
        document.body.style.background = '#56281D';
        options.x = -(currentSection-2) * (-offset + window.innerWidth);
        if(currentSection != 2){
            TweenMax.to($next, 1.2, {opacity: 1, ease: Power4.easeOut}); 
        }else{
            TweenMax.to($next, 1.2, {top: window.innerHeight * 0.5 - $next.clientHeight * 0.5, ease: Power4.easeOut}); 
            TweenMax.to($prev, 1.2, {opacity: 0, ease: Power4.easeOut});
            TweenMax.to($header, 1.2, {opacity: 0, ease: Power4.easeOut});
            TweenMax.to($mainSkip, 1.2, {opacity: 0, ease: Power4.easeOut});
        }
    }else{
        options.y = currentSection == verticalSectionIndex ? 0 : 0;
        //if(currentSection != verticalSectionIndex)
        options.x = currentSection * (-window.innerWidth + offset);
        ti = currentSection;
        if(currentSection == 1){
            TweenMax.to($next, 1.2, {top: 250, ease: Power4.easeOut}); 
            TweenMax.to($prev, 2.2, {opacity: 1, ease: Power4.easeOut});
            TweenMax.to($header, 1.2, {opacity: 1, ease: Power4.easeOut});
        }
    }
    var wheels = $bus.querySelectorAll('.wheel');
    if((currentSection == 2 && !prev) || (currentSection == 4 && prev)){
        TweenMax.to($bus, 4.8, {x: window.innerWidth - 240 - $bus.clientWidth - 200, opacity: 1, ease: Power4.easeOut});
        TweenMax.to(wheels[0], 4.8, {rotation: - 360, transformOrigin:'50% 50%', ease: Power4.easeOut});
        TweenMax.to(wheels[1], 5.5, {rotation: - 360, transformOrigin:'50% 50%', delay: 0.5, ease: Power4.easeOut});
    }else if((currentSection == 1 && !prev) || (currentSection == 3 && prev)){
        TweenMax.to($bus, 4.8, {x: 0, opacity: 1, ease: Power4.easeOut});
        TweenMax.to(wheels[0], 4.8, {rotation: 360, ease: Power4.easeOut});
        TweenMax.to(wheels[1], 5.5, {rotation: 360, delay: 0.5, ease: Power4.easeOut});
    }else
        TweenMax.to($bus, 0.5, {x: 0, opacity: 0, ease: Power4.easeOut});

    TweenMax.to($content, 1.2, options);
    var complete;
    if((prev && currentSection != 2) || (!prev && currentSection != verticalSectionIndex))
        complete = () => { if(currentSection -1 != verticalSectionIndex && $mainSkip._gsTransform.xPercent == -50) TweenMax.fromTo($mainSkip, 1.5, {opacity: 0, x: '50%'}, {opacity: 1, x: '0%', ease: Power4.easeOut}) };
    else

    timelines[ti].eventCallback('onComplete', complete);
    timelines[ti].restart();
    
    setTimeout(() => {
        document.body.style.background = '#A3ADB6';
        if(prev)
            currentSection--;
        else
            currentSection++;
        moving = false;
    }, 1200);
}
init();
window.testBrowser = function (browser) {
    var result

    switch (browser) {
    case 'safari':
        result = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === '[object SafariRemoteNotification]' })(!window['safari'] || (typeof window.safari !== 'undefined' && window.safari.pushNotification))
        break
    case 'safari mobile':
        result = /iPhone/i.test(navigator.userAgent) && /Safari/i.test(navigator.userAgent)
        break
    case 'samsung':
        result = /SamsungBrowser/.test(navigator.userAgent)
        break
    case 'chrome':
        result = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !/SamsungBrowser/.test(navigator.userAgent)
        break
    case 'chrome mobile':
        result = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !/SamsungBrowser/.test(navigator.userAgent) && !window.chrome.webstore
        break
    case 'firefox mobile':
        result = !/Chrome/.test(navigator.userAgent) && /Mozilla/.test(navigator.userAgent) && /Firefox/.test(navigator.userAgent) && /Mobile/.test(navigator.userAgent)
        break
    case 'firefox':
        result = !/Chrome/.test(navigator.userAgent) && /Mozilla/.test(navigator.userAgent) && /Firefox/.test(navigator.userAgent)
        break
    case 'ie':
        result = /MSIE/.test(window.navigator.userAgent) || /NET/.test(window.navigator.userAgent);
        break;
    case 'edge':
        result = /Edge/.test(window.navigator.userAgent);
    default:
        result = false
        break
    }
    return result
}
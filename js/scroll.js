export default class ScrollCtrl {
    constructor(onScroll){
        this.firstTouch = 0;
        this.lastDelta = 0;
        this.startTime = new Date();
        this.onScroll = onScroll;
    }
    preventDefault(e) {
        e.preventDefault()
        var deltaTime = new Date().getTime() - this.startTime
        this.startTime = new Date().getTime()
        var delta = [e.wheelDeltaX || -e.deltaX, e.wheelDeltaY || -e.deltaY];
        for (let i = 0; i < delta.length; i++) {
            if (testBrowser('firefox') && e.deltaMode === 1) {
                if (delta[i] < 0) {
                    if (delta[i] > -180) delta[i] = -180
                } else {
                    if (delta[i] < 180) delta[i] = 180
                }
            } else if (testBrowser('firefox')) {
                delta[i] -= delta[i] * 0.80
            }
            delta[i] = e.wheelDelta ? delta[i] / 120 : delta[i] / 3;
        }
        var direction = {};
        if(e.deltaX < 0)
            direction.x = 'left';
        else
            direction.x = 'right';
        
        if(e.deltaY < 0)
            direction.y = 'up';
        else
            direction.y = 'down';

        this.scroll(delta, direction)
    }

    scroll(delta, direction) {
        this.onScroll(parseInt(delta[0]*70), parseInt(delta[1]*70), direction, 'scroll');
    }
}
